import * as jsend from 'jsend'; 
import * as path from 'path';
import fs from 'fs';
import express from 'express';
import bodyParser from 'body-parser'
import helmet from 'helmet';
import cors from 'cors';
import { NextFunction } from 'connect';
import { constVariable } from './utils/const';
import { AppError } from './utils/appError';
import mainRouter from './routes'


// Start express app
const app: express.Application = express();

app.enable('trust proxy');
app.use(jsend.middleware);

// Implement CORS
app.use(cors());

// Serving static files
app.use(express.static(path.join(__dirname, 'public')));

// Body parser, reading data from body into req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Set security HTTP headers
app.use(helmet());


// Add headers
app.use(function (req:any, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    next();
});


app.use((req: any, res: any, next: any) => {
  if( req.originalUrl == '/api/v1/user/login' && req.method != 'POST'){
    return res.status(constVariable.HTTP.NOTFOUND).json({status:"ERROR",message:`Can't find ${req.originalUrl} on this server!`})
  }
  next();
})


// Test middleware
app.use((req: any, res: any, next: NextFunction) => {
  req.requestTime = new Date().toISOString();
  next();
});

app.use('/', mainRouter)

app.use('/', (req:any , res:any) => { 
  return res.status(constVariable.HTTP.NOTFOUND).json({statusCode: constVariable.HTTP.NOTFOUND, status:"ERROR",message:`Can't find ${req.originalUrl} on this server!`})
})
app.all('*', (req: any, res: any, next: any) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, constVariable.HTTP.NOTFOUND));
});

export default app;
