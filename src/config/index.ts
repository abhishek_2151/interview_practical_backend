import { config } from 'dotenv';
import databaseConfig from './database';
config({ path: `.env.${process.env.NODE_ENV || 'development'}` });

export const CREDENTIALS = process.env.CREDENTIALS === 'true';
export const {
  NODE_ENV,
  DB_USERNAME,
  DB_PASSWORD,
  DB_DATABASE,
  DB_PORT,
  DB_HOST,
  JWT_SECRET,
  LANGUAGE_TYPE,
} = process.env;

export default {
  ...databaseConfig
}

