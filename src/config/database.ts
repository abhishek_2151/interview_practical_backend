export const databaseSchemaNames = {
  audioSchema: 'Audio',
  userSchema: 'User'
};


export const schemaOptions = {
  versionKey: false,
  timestamps: {
    createdAt: true,
    updatedAt: 'modifiedAt',
  },
};

export const defaultListingParameter = {
  query: {},
  projection: '',
  skip: 0,
  sort: '-_id',
  limit: 10,
  population: '',
  populationArray: [],
};


export default {
  databaseSchemaNames,
};

