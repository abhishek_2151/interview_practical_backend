const multer = require('multer');
const fs = require('fs');
const folderName = 'uploads/';
const ffmpeg = require('fluent-ffmpeg');
import { constVariable } from "../utils/const";
import { msg } from "../server";
if (!fs.existsSync(folderName)) fs.mkdirSync(folderName)


const storage = multer.diskStorage({
    destination: function (req: any, file: any, cb: any) {
      let dir = `./uploads/audio`
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true })
      }
      cb(null, dir);
    },
    filename: function (req: any, file: any, cb: any) {
      cb(null, file.originalname.replace(/\s/g, ''));
    },
  });
  

export const upload = multer({
    storage: storage,
    fileFilter: (req: any, file: any, cb: any) => {

      ffmpeg.ffprobe(file.path, (err:any, metadata:any) => {
        if (err) {
          return cb(new Error(msg.fileValidation.invalidFormat));
        }
    
        const durationInSeconds = metadata.format.duration;
    
        if (durationInSeconds < 30) {
          return cb(new Error(msg.fileValidation.invalidSize));
        }
      });
      cb(null, true);
    },
  });
