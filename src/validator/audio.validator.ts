import * as Joi from "joi";
import { msg }from '../server';

export const addAudio = Joi.object({
    name: Joi.string().trim().required().messages(msg.joi),
    description: Joi.string().trim().required().messages(msg.joi),
    image: Joi.string().trim().required().messages(msg.joi)
})

export const updateAudio = Joi.object({
    name: Joi.string().trim().required().messages(msg.joi),
    description: Joi.string().trim().required().messages(msg.joi),
    image: Joi.string().trim().required().messages(msg.joi)
})

export default {
    addAudio,
    updateAudio
}
