import {LANGUAGE_TYPE} from './config/index';
const pathToModule = `./languages/${LANGUAGE_TYPE}`;
const  { lang }   = require(`${pathToModule}`)
export let msg = lang;
import { dbConnection } from './databases';
import { connect } from 'mongoose';
import app from './app';
import http from 'http';
import DefaultData from './migration/defaultData';

export const db:any = connect(dbConnection.url, dbConnection.options).then(() => console.log(lang.dbConnSuccessMsg));
(async() => {
  await new DefaultData().createDefaultRoles();
})()

const port = process.env.PORT ;
const server = http.createServer(app);
server.listen(port, () => {
   console.log(`${lang.serverListenMsg} ${port}...`);
});
