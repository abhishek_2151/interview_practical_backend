import mongoose from 'mongoose';
import { msg } from '../server';
import { 
    databaseSchemaNames, 
    schemaOptions
} from '../config/database';

const audioSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, msg.module.insertAudioName],
            unique: [true, msg.module.audioNameExist]
        },
        image:{
            type: String,
        },
        description: {
            type:String,
            required: [true, msg.module.insertDescription],
        },
        audioUrl: {
            type: String
        }
    },
    schemaOptions
);

const Audio = mongoose.model(databaseSchemaNames.audioSchema, audioSchema);

export default Audio;


