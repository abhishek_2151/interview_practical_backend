import AudioModel from "./audioModel"
import UserModel from "./userModel"

const schemaExport = {
    AudioModel,
    UserModel
}

export {
    AudioModel,
    UserModel
}

export default schemaExport
