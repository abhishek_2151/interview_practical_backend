import { UserModel } from '../models';
import { defaultListingParameter } from '../config/database';


export const findOne = async ({ query = defaultListingParameter.query}) => {

  return await UserModel.findOne({...query }).lean()
}


export default {
    findOne
}
