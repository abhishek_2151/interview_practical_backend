# backend
## INTRODUCTION
   *  This projects contains the boilerplate of any typescript backend projects. We can start the project from here only.
## Prerequisites
1. Install Node.js
2. Install MongoDB
3. ts-node should be install globally (npm  i  g ts-node)

* versions:
    * Node: v16.14.1,
    * MongoDB: v5.0,
    * Express: ^4.0,
    * swagger-ui-express: "^4.3.0"

This project users the Node.js
* **M**ongoose.js (MongoDB): database
* **E**xpress.js: backend framework
* **N**ode.js: runtime environment

Other tools and technologies used:
* JSON Web Token: user authentication
* NodeMailer:email service
* bcrypt:password encryption
* swagger-ui-express:swagger ui
* express-basic-auth:Basic authentication
* Whatsapp authentication with broadcast message
* How to run:
    * first you get the git code edit the ".env.{{development}}" based on your server setup environment and then use below command.
    * Use Command to run the code.
          1. From project root folder install all the dependencies: `npm i`,
          2. "npm run dev" for development setup,      
          3.  Now this app is  running on (http://localhost:5000/),
          4. swagger url : (http://localhost:5000/api-docs/#/) 
               * swaggerCredential
                  * userName : Admin,
                  * password : admin@123,
## Server configuration
Staging Frontend: https://originoils.infistack.net/login
Login(Super Admin) credentials : 9664531509 / 333333
Login(Field Agent) credentials : 7338494625 / 333333
Production Backend : https://ooapi.infistack.net/api-docs/#/
Credentials : Admin / admin@123

## Logger
* Winston and morgan used for logger management.You can check logs at below path.
	1. Path : src/logs

## Default Role/Permission/User data
* Using below script we have created two role and users with permission.
	1. Path : src/migration/defaultData.ts

* roles detail
1. Super admin
2. Area admin
3. Field agent

* Default super admin user logged in detail
1. Mobile : 9664531509 
2. OTP: 333333

## Creating mongodb user via commandline
```
db.createUser(
  {
    user: "infi",
    pwd: "infi@111",  // or cleartext password
    roles: [
       { role: "readWrite", db: "originOilsStage" }
    ]
  }
)
```

## Mongoose Connection
* You can find mongodb connection string from below path
1. Path : src/databases

## Const variable declaration
1. Environment variable file path : src/config
2. Constant variable file path : src/utils/const.ts
3. Error message file path : src/utils/en-in.ts
4. Endpoint file path : src/utils/endpoints.ts
5. Language file path : src/languages/en.ts
## Cron Agenda configuration
- file path/to/ src/crons/index.ts
  - Default schedule configuration set in environment file.
## Deployment configuration file
- file path/to/ dist/crons/agenda.cron.js
  - Note: Please do not change this file until deployment port is not available.Default port will be 5000.
## Deployment configuration tools
- Pm2 Installation Commands: npm install pm2 -g
  - Note: We have used node js pm2 server configuration for the deployment and domain configuration using nginx server.
## Deploy
- clone repository.
- Go to repository root path
- Check and verify environments configuration file
- from project root repository path : npm install
- Generate angular application build using from project root repository path : npm run build
   -  Note: Please do not modify build configuration.If you want to change the build configuration, then you have also to change  the package.json configuration file
- from project root repository path : npm run stage-cron
   - Note: Using this steps, Node application will be deploy successfully.
### Pm2 Commands

Use the well known command to copy the template

```bash
# To list all running applications
 pm2 list

# Restart pm2 server
 pm2 restart  <originOils-frontend>
 
# Stop pm2 server
 pm2 stop  <originOils-frontend>

# Delete pm2 server
 pm2 delete  <originOils-frontend>
 
# Check all logs
 pm2 logs

# Check specific pm2 server log
 pm2 log <originOils-frontend>

```

## Important notes
1. Address static JSON file need to import.We can download file from Master > Address Static
2. Permission  Excel file need to import.We can download file from Master > Permission
